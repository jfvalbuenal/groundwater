#source("recharge.R")
gws = datos[["SerieDeTiempo_GWS_tavg"]]
## Aplicación de exploración para cálculo de recarga
# Vector calculo de reacarga anual
recarga_anual = vector()
years = seq(2003,2021,1)
paste(years,"-12-30",sep = "")

for (year in years) {

fecha_ini =paste(year,"-12-30",sep = "")
fecha_fin = paste(year + 1,"-12-30",sep = "")
p = gws [gws$ID_Punto == 'P_18',]
p = p[!is.na(p$GWS_tavg),]
# Configurar índice temporal
date_min = min(p$Time)
gr = ts(p$GWS_tavg,frequency = 365,start = c(2003,32))# Año y numero de día
a=ts_to_prophet(gr)
tedencia=ts_to_prophet(gr)
ts_info(gr)
p_decompouse = decompose(gr)
# Extraer la estacionalidad
estacionalidad=ts_to_prophet(p_decompouse[["x"]])

plot(p_decompouse)
# Generar una subserie con un 'ciclo típico' de acuerdo con la estacionalidad
tipico = estacionalidad[estacionalidad$ds >=fecha_ini,]
tipico = tipico[tipico$ds <=fecha_fin,];plot(tipico)
tipico = ts(tipico$y,start = 1)
index_min = which.min(tipico)
# Identificar el día en que se da el valor maximo despues del punto de mayor inflexión en gws
if (which.max(tipico) >= dim(p)[1]) {
  index_max = dim(p)[1]
}else{
  index_max = which.max(tipico)
}


# Generar subserie para tendencia
tipico_min=tipico[50:index_min-50]
tipico_max = tipico[index_min:length(tipico)]
# Establecer día en el que se presentaría el valor máximo de gws despues de
# alcanzar el mínimo en el ciclo que se evalua
index_max = index_min + which.max(tipico_max)
plot(tipico_min,type = 'l')
# Generar modelo lineal
summary(fit <- lm(formula = tipico_min ~ time(tipico_min), na.action = NULL))


# Ver tendencia
plot(tipico_min, ylab="gws") 
abline(fit,col = "red") # Se añade la recta ajustada
# Aplicación de un modelo lineal simple para estimar el valor que tendría el gws
# para la fecha en la que se presenta el valor máximo de esta variable si 
# el fenómeno siguiera la tendencia lineal decreciente que ha sido calculada.

valmin = (index_max * fit[["coefficients"]][2]) + fit[["coefficients"]][1]

#
#
#
# Datos con fechas y variables. La columna 'date' (fecha) es de clase "Date"
p = p[p$Time >=fecha_ini & p$Time <=fecha_fin,]

# Crear objeto para contruir linea de tendencia hasta fecha de valor maximo
pred = (1 * fit[["coefficients"]][2]) + fit[["coefficients"]][1]
pred = append(pred,(index_max * fit[["coefficients"]][2]) + fit[["coefficients"]][1])
fechas = as.Date(c(min(p$Time),min(p$Time)+index_max))

pred =data_frame(Valores = pred, Fechas= fechas)
# Grafica
ggplot(data = p,aes(Time, GWS_tavg))  + geom_line(size=2) +
  # Calculo de R_s
  geom_linerange(aes(ymax=GWS_tavg, 
                     ymin=min(p$GWS_tavg)),
                 data=p[index_max-1,],
                 col="red", lty=2)+
  # Calculo de R_d
  geom_linerange(aes(ymax=min(p$GWS_tavg), ymin=valmin),
                 data=p[index_max-1,],
                 col="red4", lty=2)+
  
  geom_hline(yintercept = min(p$GWS_tavg),col="gray30",lty=2)+
  # Tendencia calculada para estimar valor mínimo en fecha de valor máximo 
  geom_line(data = pred, aes(x = Fechas, y = Valores),col="blue3",lty=2)+
  # Establecer títulos de gráfico
  ggtitle("Recarga anual calulada a partir de almacenamiento de Agua Subterránea",subtitle = "Fuente: GRACE - GLDAS") +
  xlab("") +
  ylab("Almacenamiento total de agua suberranea (mm)")+
  theme_light()

recarga=p[index_max-1,6] - valmin

recarga_anual = append(recarga_anual,recarga)

}
#  Crear tabla con valores

cbind(years,recarga_anual,'P_26')
